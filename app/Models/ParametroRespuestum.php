<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParametroRespuestum extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'parametro_respuesta';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'respuesta_id',
        'valor',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function respuesta()
    {
        return $this->belongsTo(Respuestum::class, 'respuesta_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
