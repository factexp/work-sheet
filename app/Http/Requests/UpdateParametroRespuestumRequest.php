<?php

namespace App\Http\Requests;

use App\Models\ParametroRespuestum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateParametroRespuestumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('parametro_respuestum_edit');
    }

    public function rules()
    {
        return [];
    }
}
