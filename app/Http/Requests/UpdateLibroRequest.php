<?php

namespace App\Http\Requests;

use App\Models\Libro;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateLibroRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('libro_edit');
    }

    public function rules()
    {
        return [
            'nombre' => [
                'string',
                'nullable',
            ],
            'descripcion' => [
                'string',
                'nullable',
            ],
        ];
    }
}
