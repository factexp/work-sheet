<?php

namespace App\Http\Requests;

use App\Models\Representante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRepresentanteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('representante_create');
    }

    public function rules()
    {
        return [];
    }
}
