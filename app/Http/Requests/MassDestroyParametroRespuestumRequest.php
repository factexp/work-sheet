<?php

namespace App\Http\Requests;

use App\Models\ParametroRespuestum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyParametroRespuestumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('parametro_respuestum_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:parametro_respuesta,id',
        ];
    }
}
