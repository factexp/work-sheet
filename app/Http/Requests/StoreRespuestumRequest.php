<?php

namespace App\Http\Requests;

use App\Models\Respuestum;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRespuestumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('respuestum_create');
    }

    public function rules()
    {
        return [];
    }
}
