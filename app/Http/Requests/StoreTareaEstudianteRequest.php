<?php

namespace App\Http\Requests;

use App\Models\TareaEstudiante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreTareaEstudianteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tarea_estudiante_create');
    }

    public function rules()
    {
        return [
            'tarea_id' => [
                'required',
                'integer',
            ],
            'valor' => [
                'string',
                'nullable',
            ],
        ];
    }
}
