<?php

namespace App\Http\Requests;

use App\Models\CodigoLibro;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCodigoLibroRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('codigo_libro_create');
    }

    public function rules()
    {
        return [];
    }
}
