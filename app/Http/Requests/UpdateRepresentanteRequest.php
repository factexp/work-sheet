<?php

namespace App\Http\Requests;

use App\Models\Representante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRepresentanteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('representante_edit');
    }

    public function rules()
    {
        return [];
    }
}
