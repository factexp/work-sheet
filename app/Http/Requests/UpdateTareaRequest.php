<?php

namespace App\Http\Requests;

use App\Models\Tarea;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTareaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tarea_edit');
    }

    public function rules()
    {
        return [
            'nombre' => [
                'string',
                'required',
            ],
        ];
    }
}
