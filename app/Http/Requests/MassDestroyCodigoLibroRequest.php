<?php

namespace App\Http\Requests;

use App\Models\CodigoLibro;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCodigoLibroRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('codigo_libro_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:codigo_libros,id',
        ];
    }
}
