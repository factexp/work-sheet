<?php

namespace App\Http\Requests;

use App\Models\TareaEstudiante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTareaEstudianteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('tarea_estudiante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:tarea_estudiantes,id',
        ];
    }
}
