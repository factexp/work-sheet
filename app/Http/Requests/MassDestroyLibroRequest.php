<?php

namespace App\Http\Requests;

use App\Models\Libro;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyLibroRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('libro_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:libros,id',
        ];
    }
}
