<?php

namespace App\Http\Requests;

use App\Models\CodigoEstudiante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCodigoEstudianteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('codigo_estudiante_create');
    }

    public function rules()
    {
        return [
            'codigo' => [
                'string',
                'nullable',
            ],
        ];
    }
}
