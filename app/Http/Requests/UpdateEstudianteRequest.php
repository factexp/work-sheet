<?php

namespace App\Http\Requests;

use App\Models\Estudiante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateEstudianteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('estudiante_edit');
    }

    public function rules()
    {
        return [];
    }
}
