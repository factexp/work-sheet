<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTareaEstudianteRequest;
use App\Http\Requests\StoreTareaEstudianteRequest;
use App\Http\Requests\UpdateTareaEstudianteRequest;
use App\Models\Tarea;
use App\Models\TareaEstudiante;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TareaEstudianteController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('tarea_estudiante_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $tareas = Tarea::with(['profesor', 'materia', 'curso', 'libro', 'created_by', 'media'])->get();

        $tareaEstudiantes = TareaEstudiante::with(['tarea', 'estudiante', 'profesor', 'curso', 'materia', 'created_by'])->get();

        return view('admin.tareaEstudiantes.index', compact('tareaEstudiantes', 'tareas'));
    }

    public function create()
    {
        abort_if(Gate::denies('tarea_estudiante_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareas = Tarea::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.tareaEstudiantes.create', compact('tareas'));
    }

    public function store(StoreTareaEstudianteRequest $request)
    {
        $tareaEstudiante = TareaEstudiante::create($request->all());

        return redirect()->route('admin.tarea-estudiantes.index');
    }

    public function edit(TareaEstudiante $tareaEstudiante)
    {
        abort_if(Gate::denies('tarea_estudiante_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareas = Tarea::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $tareaEstudiante->load('tarea', 'estudiante', 'profesor', 'curso', 'materia', 'created_by');

        return view('admin.tareaEstudiantes.edit', compact('tareas', 'tareaEstudiante'));
    }

    public function update(UpdateTareaEstudianteRequest $request, TareaEstudiante $tareaEstudiante)
    {
        $tareaEstudiante->update($request->all());

        return redirect()->route('admin.tarea-estudiantes.index');
    }

    public function show(TareaEstudiante $tareaEstudiante)
    {
        abort_if(Gate::denies('tarea_estudiante_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareaEstudiante->load('tarea', 'estudiante', 'profesor', 'curso', 'materia', 'created_by');

        return view('admin.tareaEstudiantes.show', compact('tareaEstudiante'));
    }

    public function destroy(TareaEstudiante $tareaEstudiante)
    {
        abort_if(Gate::denies('tarea_estudiante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareaEstudiante->delete();

        return back();
    }

    public function massDestroy(MassDestroyTareaEstudianteRequest $request)
    {
        TareaEstudiante::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
