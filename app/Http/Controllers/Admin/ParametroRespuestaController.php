<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyParametroRespuestumRequest;
use App\Http\Requests\StoreParametroRespuestumRequest;
use App\Http\Requests\UpdateParametroRespuestumRequest;
use App\Models\ParametroRespuestum;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ParametroRespuestaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('parametro_respuestum_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parametroRespuesta = ParametroRespuestum::with(['respuesta'])->get();

        return view('admin.parametroRespuesta.index', compact('parametroRespuesta'));
    }

    public function create()
    {
        abort_if(Gate::denies('parametro_respuestum_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parametroRespuesta.create');
    }

    public function store(StoreParametroRespuestumRequest $request)
    {
        $parametroRespuestum = ParametroRespuestum::create($request->all());

        return redirect()->route('admin.parametro-respuesta.index');
    }

    public function edit(ParametroRespuestum $parametroRespuestum)
    {
        abort_if(Gate::denies('parametro_respuestum_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parametroRespuestum->load('respuesta');

        return view('admin.parametroRespuesta.edit', compact('parametroRespuestum'));
    }

    public function update(UpdateParametroRespuestumRequest $request, ParametroRespuestum $parametroRespuestum)
    {
        $parametroRespuestum->update($request->all());

        return redirect()->route('admin.parametro-respuesta.index');
    }

    public function show(ParametroRespuestum $parametroRespuestum)
    {
        abort_if(Gate::denies('parametro_respuestum_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parametroRespuestum->load('respuesta');

        return view('admin.parametroRespuesta.show', compact('parametroRespuestum'));
    }

    public function destroy(ParametroRespuestum $parametroRespuestum)
    {
        abort_if(Gate::denies('parametro_respuestum_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parametroRespuestum->delete();

        return back();
    }

    public function massDestroy(MassDestroyParametroRespuestumRequest $request)
    {
        ParametroRespuestum::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
