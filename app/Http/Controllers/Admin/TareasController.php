<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTareaRequest;
use App\Http\Requests\StoreTareaRequest;
use App\Http\Requests\UpdateTareaRequest;
use App\Models\Curso;
use App\Models\Libro;
use App\Models\Materium;
use App\Models\Tarea;
use App\Models\User;
use App\Models\Respuestum;
use App\Models\ParametroRespuestum;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TareasController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('tarea_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareas = Tarea::with(['profesor', 'materia', 'curso', 'libro', 'created_by', 'media'])->get();

        return view('admin.tareas.index', compact('tareas'));
    }

    public function create()
    {
        abort_if(Gate::denies('tarea_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $profesors = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $materias = Materium::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $libros = Libro::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.tareas.create', compact('profesors', 'materias', 'cursos', 'libros'));
    }

    public function store(StoreTareaRequest $request)
    {
        $tarea = Tarea::create($request->all());

        if ($request->input('archivo', false)) {
            $tarea->addMedia(storage_path('tmp/uploads/' . basename($request->input('archivo'))))->toMediaCollection('archivo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $tarea->id]);
        }

        return redirect()->route('admin.tareas.index');
    }

    public function edit(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $profesors = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $materias = Materium::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $libros = Libro::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $tarea->load('profesor', 'materia', 'curso', 'libro', 'created_by');

        return view('admin.tareas.edit', compact('profesors', 'materias', 'cursos', 'libros', 'tarea'));
    }

    public function update(UpdateTareaRequest $request, Tarea $tarea)
    {
        $tarea->update($request->all());

        if ($request->input('archivo', false)) {
            if (!$tarea->archivo || $request->input('archivo') !== $tarea->archivo->file_name) {
                if ($tarea->archivo) {
                    $tarea->archivo->delete();
                }
                $tarea->addMedia(storage_path('tmp/uploads/' . basename($request->input('archivo'))))->toMediaCollection('archivo');
            }
        } elseif ($tarea->archivo) {
            $tarea->archivo->delete();
        }

        return redirect()->route('admin.tareas.index');
    }

    public function show(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tarea->load('profesor', 'materia', 'curso', 'libro', 'created_by');

        return view('admin.tareas.show', compact('tarea'));
    }

    public function destroy(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tarea->delete();

        return back();
    }

    public function massDestroy(MassDestroyTareaRequest $request)
    {
        Tarea::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('tarea_create') && Gate::denies('tarea_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Tarea();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }

    public function addTarea(Request $request)
    { 
        $idtarea = $request->id;
        //$tareas = Tarea::with(['profesor', 'materia', 'curso', 'libro', 'created_by', 'media'])->get();       
        return view('admin.tareas.addtarea', compact('idtarea'));
    }
    public function editorTarea(Request $request)
    {                  
        $idTarea    = $request->idtarea;
        $tarea      = Tarea::where('id','=' ,$idTarea)->first();
        $urlArchivo = str_replace('http://localhost/', 'http://ec2-52-67-251-240.sa-east-1.compute.amazonaws.com/work-sheet/public/', $tarea->archivo->getUrl());
        
        return view('admin.tareas.editor', compact('urlArchivo'));
    }

    public function storeTareaDetalle(Request $request)
    { 
        $idTarea = $request->idtarea;
        $arrayContenidoRespuesta = json_decode($request->contenidoguardarFormularioSave);
        
        foreach ( $arrayContenidoRespuesta as $contenidoRespuesta)
        {
            
            $respuesta = new Respuestum;
            $respuesta->descripcion = $contenidoRespuesta[0];
            $respuesta->tarea_id = $idTarea;
            $respuesta->save();
            
           
            $primeravuelta = true;
            foreach ( $contenidoRespuesta as $parametro)
            {
                
                if($primeravuelta){
                    $primeravuelta = false;
                    continue;
                }             
                $parametroRespuesta               = new ParametroRespuestum;
                $parametroRespuesta->respuesta_id = $respuesta->id;
                $parametroRespuesta->valor        = $parametro;
                $parametroRespuesta->save();
            }
        }       
        
       
        //$tareas = Tarea::with(['profesor', 'materia', 'curso', 'libro', 'created_by', 'media'])->get();

        return redirect()->route('admin.tareas.index');
    }

    public function realizarTareaEstudiante(Request $request)
    {         
        $idtarea                = $request->idtarea;
        $respuesta              = array();    
        $contenidoRespuesta    = array();     
        $tarea                  = Tarea::where('id','=' ,$idtarea)->first();
        $respuestasTarea         = Respuestum::where('tarea_id','=', $tarea->id)->get();
        
        foreach($respuestasTarea as  $respuestaTarea)
        {
            $respuesta              = array();
            array_push($respuesta, $respuestaTarea->descripcion);
            $parametroRespuesta = ParametroRespuestum::where('respuesta_id','=', $respuestaTarea->id)->get();        
            foreach($parametroRespuesta as  $parametro)
            {
                array_push($respuesta,$parametro->valor);
            }
            array_push($contenidoRespuesta, $respuesta);
        }         
        $urlArchivo         = str_replace('http://localhost/', 'http://ec2-52-67-251-240.sa-east-1.compute.amazonaws.com/work-sheet/public/', $tarea->archivo->getUrl());
        return view('admin.tareas.realizartarea', compact('idtarea', 'contenidoRespuesta', 'urlArchivo'));
    }
    
}
