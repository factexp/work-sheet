<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Respuestum;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RespuestasController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('respuestum_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $respuesta = Respuestum::with(['tarea'])->get();

        return view('admin.respuesta.index', compact('respuesta'));
    }
}
