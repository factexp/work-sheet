<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMateriumRequest;
use App\Http\Requests\StoreMateriumRequest;
use App\Http\Requests\UpdateMateriumRequest;
use App\Models\Curso;
use App\Models\Materium;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MateriaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('materium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $materia = Materium::with(['curso', 'created_by'])->get();

        return view('admin.materia.index', compact('materia'));
    }

    public function create()
    {
        abort_if(Gate::denies('materium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.materia.create', compact('cursos'));
    }

    public function store(StoreMateriumRequest $request)
    {
        $materium = Materium::create($request->all());

        return redirect()->route('admin.materia.index');
    }

    public function edit(Materium $materium)
    {
        abort_if(Gate::denies('materium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $materium->load('curso', 'created_by');

        return view('admin.materia.edit', compact('cursos', 'materium'));
    }

    public function update(UpdateMateriumRequest $request, Materium $materium)
    {
        $materium->update($request->all());

        return redirect()->route('admin.materia.index');
    }

    public function show(Materium $materium)
    {
        abort_if(Gate::denies('materium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $materium->load('curso', 'created_by');

        return view('admin.materia.show', compact('materium'));
    }

    public function destroy(Materium $materium)
    {
        abort_if(Gate::denies('materium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $materium->delete();

        return back();
    }

    public function massDestroy(MassDestroyMateriumRequest $request)
    {
        Materium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
