<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCodigoLibroRequest;
use App\Models\CodigoLibro;
use App\Models\Libro;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CodigoLibroController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('codigo_libro_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $codigoLibros = CodigoLibro::with(['libro'])->get();

        return view('admin.codigoLibros.index', compact('codigoLibros'));
    }

    public function create()
    {
        abort_if(Gate::denies('codigo_libro_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libros = Libro::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.codigoLibros.create', compact('libros'));
    }

    public function store(StoreCodigoLibroRequest $request)
    {
        $codigoLibro = CodigoLibro::create($request->all());

        return redirect()->route('admin.codigo-libros.index');
    }
}
