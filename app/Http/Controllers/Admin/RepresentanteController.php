<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRepresentanteRequest;
use App\Http\Requests\StoreRepresentanteRequest;
use App\Http\Requests\UpdateRepresentanteRequest;
use App\Models\Representante;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RepresentanteController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('representante_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $representantes = Representante::with(['estudiante', 'representante'])->get();

        return view('admin.representantes.index', compact('representantes'));
    }

    public function create()
    {
        abort_if(Gate::denies('representante_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estudiantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $representantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.representantes.create', compact('estudiantes', 'representantes'));
    }

    public function store(StoreRepresentanteRequest $request)
    {
        $representante = Representante::create($request->all());

        return redirect()->route('admin.representantes.index');
    }

    public function edit(Representante $representante)
    {
        abort_if(Gate::denies('representante_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estudiantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $representantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $representante->load('estudiante', 'representante');

        return view('admin.representantes.edit', compact('estudiantes', 'representantes', 'representante'));
    }

    public function update(UpdateRepresentanteRequest $request, Representante $representante)
    {
        $representante->update($request->all());

        return redirect()->route('admin.representantes.index');
    }

    public function show(Representante $representante)
    {
        abort_if(Gate::denies('representante_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $representante->load('estudiante', 'representante');

        return view('admin.representantes.show', compact('representante'));
    }

    public function destroy(Representante $representante)
    {
        abort_if(Gate::denies('representante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $representante->delete();

        return back();
    }

    public function massDestroy(MassDestroyRepresentanteRequest $request)
    {
        Representante::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
