<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEstudianteRequest;
use App\Http\Requests\StoreEstudianteRequest;
use App\Http\Requests\UpdateEstudianteRequest;
use App\Models\Curso;
use App\Models\Estudiante;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EstudianteController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('estudiante_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estudiantes = Estudiante::with(['usuario', 'curso'])->get();

        return view('admin.estudiantes.index', compact('estudiantes'));
    }

    public function create()
    {
        abort_if(Gate::denies('estudiante_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.estudiantes.create', compact('usuarios', 'cursos'));
    }

    public function store(StoreEstudianteRequest $request)
    {
        $estudiante = Estudiante::create($request->all());

        return redirect()->route('admin.estudiantes.index');
    }

    public function edit(Estudiante $estudiante)
    {
        abort_if(Gate::denies('estudiante_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $estudiante->load('usuario', 'curso');

        return view('admin.estudiantes.edit', compact('usuarios', 'cursos', 'estudiante'));
    }

    public function update(UpdateEstudianteRequest $request, Estudiante $estudiante)
    {
        $estudiante->update($request->all());

        return redirect()->route('admin.estudiantes.index');
    }

    public function show(Estudiante $estudiante)
    {
        abort_if(Gate::denies('estudiante_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estudiante->load('usuario', 'curso');

        return view('admin.estudiantes.show', compact('estudiante'));
    }

    public function destroy(Estudiante $estudiante)
    {
        abort_if(Gate::denies('estudiante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estudiante->delete();

        return back();
    }

    public function massDestroy(MassDestroyEstudianteRequest $request)
    {
        Estudiante::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
