<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyLibroRequest;
use App\Http\Requests\StoreLibroRequest;
use App\Http\Requests\UpdateLibroRequest;
use App\Models\Curso;
use App\Models\Libro;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LibroController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('libro_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libros = Libro::with(['curso'])->get();

        return view('admin.libros.index', compact('libros'));
    }

    public function create()
    {
        abort_if(Gate::denies('libro_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.libros.create', compact('cursos'));
    }

    public function store(StoreLibroRequest $request)
    {
        $libro = Libro::create($request->all());

        return redirect()->route('admin.libros.index');
    }

    public function edit(Libro $libro)
    {
        abort_if(Gate::denies('libro_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $libro->load('curso');

        return view('admin.libros.edit', compact('cursos', 'libro'));
    }

    public function update(UpdateLibroRequest $request, Libro $libro)
    {
        $libro->update($request->all());

        return redirect()->route('admin.libros.index');
    }

    public function show(Libro $libro)
    {
        abort_if(Gate::denies('libro_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libro->load('curso');

        return view('admin.libros.show', compact('libro'));
    }

    public function destroy(Libro $libro)
    {
        abort_if(Gate::denies('libro_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libro->delete();

        return back();
    }

    public function massDestroy(MassDestroyLibroRequest $request)
    {
        Libro::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
