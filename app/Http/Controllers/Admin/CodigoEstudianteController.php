<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCodigoEstudianteRequest;
use App\Http\Requests\StoreCodigoEstudianteRequest;
use App\Http\Requests\UpdateCodigoEstudianteRequest;
use App\Models\CodigoEstudiante;
use App\Models\Libro;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CodigoEstudianteController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('codigo_estudiante_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $codigoEstudiantes = CodigoEstudiante::with(['libro', 'estudiante'])->get();

        return view('admin.codigoEstudiantes.index', compact('codigoEstudiantes'));
    }

    public function create()
    {
        abort_if(Gate::denies('codigo_estudiante_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libros = Libro::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $estudiantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.codigoEstudiantes.create', compact('libros', 'estudiantes'));
    }

    public function store(StoreCodigoEstudianteRequest $request)
    {
        $codigoEstudiante = CodigoEstudiante::create($request->all());

        return redirect()->route('admin.codigo-estudiantes.index');
    }

    public function edit(CodigoEstudiante $codigoEstudiante)
    {
        abort_if(Gate::denies('codigo_estudiante_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $libros = Libro::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $estudiantes = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $codigoEstudiante->load('libro', 'estudiante');

        return view('admin.codigoEstudiantes.edit', compact('libros', 'estudiantes', 'codigoEstudiante'));
    }

    public function update(UpdateCodigoEstudianteRequest $request, CodigoEstudiante $codigoEstudiante)
    {
        $codigoEstudiante->update($request->all());

        return redirect()->route('admin.codigo-estudiantes.index');
    }

    public function show(CodigoEstudiante $codigoEstudiante)
    {
        abort_if(Gate::denies('codigo_estudiante_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $codigoEstudiante->load('libro', 'estudiante');

        return view('admin.codigoEstudiantes.show', compact('codigoEstudiante'));
    }

    public function destroy(CodigoEstudiante $codigoEstudiante)
    {
        abort_if(Gate::denies('codigo_estudiante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $codigoEstudiante->delete();

        return back();
    }

    public function massDestroy(MassDestroyCodigoEstudianteRequest $request)
    {
        CodigoEstudiante::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
