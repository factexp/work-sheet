@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.representante.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.representantes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="estudiante_id">{{ trans('cruds.representante.fields.estudiante') }}</label>
                <select class="form-control select2 {{ $errors->has('estudiante') ? 'is-invalid' : '' }}" name="estudiante_id" id="estudiante_id">
                    @foreach($estudiantes as $id => $entry)
                        <option value="{{ $id }}" {{ old('estudiante_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('estudiante'))
                    <span class="text-danger">{{ $errors->first('estudiante') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.representante.fields.estudiante_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="representante_id">{{ trans('cruds.representante.fields.representante') }}</label>
                <select class="form-control select2 {{ $errors->has('representante') ? 'is-invalid' : '' }}" name="representante_id" id="representante_id">
                    @foreach($representantes as $id => $entry)
                        <option value="{{ $id }}" {{ old('representante_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('representante'))
                    <span class="text-danger">{{ $errors->first('representante') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.representante.fields.representante_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection