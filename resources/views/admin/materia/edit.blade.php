@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.materium.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.materia.update", [$materium->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="nombre">{{ trans('cruds.materium.fields.nombre') }}</label>
                <input class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" type="text" name="nombre" id="nombre" value="{{ old('nombre', $materium->nombre) }}" required>
                @if($errors->has('nombre'))
                    <span class="text-danger">{{ $errors->first('nombre') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.materium.fields.nombre_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="curso_id">{{ trans('cruds.materium.fields.curso') }}</label>
                <select class="form-control select2 {{ $errors->has('curso') ? 'is-invalid' : '' }}" name="curso_id" id="curso_id">
                    @foreach($cursos as $id => $entry)
                        <option value="{{ $id }}" {{ (old('curso_id') ? old('curso_id') : $materium->curso->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('curso'))
                    <span class="text-danger">{{ $errors->first('curso') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.materium.fields.curso_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection