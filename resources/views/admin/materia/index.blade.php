@extends('layouts.admin')
@section('content')
@can('materium_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.materia.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.materium.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.materium.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Materium">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.materium.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.materium.fields.nombre') }}
                        </th>
                        <th>
                            {{ trans('cruds.materium.fields.curso') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($materia as $key => $materium)
                        <tr data-entry-id="{{ $materium->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $materium->id ?? '' }}
                            </td>
                            <td>
                                {{ $materium->nombre ?? '' }}
                            </td>
                            <td>
                                {{ $materium->curso->nombre ?? '' }}
                            </td>
                            <td>
                                @can('materium_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.materia.show', $materium->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('materium_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.materia.edit', $materium->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('materium_delete')
                                    <form action="{{ route('admin.materia.destroy', $materium->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('materium_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.materia.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Materium:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection