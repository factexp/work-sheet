@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.codigoLibro.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.codigo-libros.update", [$codigoLibro->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="libro_id">{{ trans('cruds.codigoLibro.fields.libro') }}</label>
                <select class="form-control select2 {{ $errors->has('libro') ? 'is-invalid' : '' }}" name="libro_id" id="libro_id">
                    @foreach($libros as $id => $entry)
                        <option value="{{ $id }}" {{ (old('libro_id') ? old('libro_id') : $codigoLibro->libro->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('libro'))
                    <span class="text-danger">{{ $errors->first('libro') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoLibro.fields.libro_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection