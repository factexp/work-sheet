@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.codigoLibro.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.codigo-libros.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="libro_id">{{ trans('cruds.codigoLibro.fields.libro') }}</label>
                <select class="form-control select2 {{ $errors->has('libro') ? 'is-invalid' : '' }}" name="libro_id" id="libro_id">
                    @foreach($libros as $id => $entry)
                        <option value="{{ $id }}" {{ old('libro_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('libro'))
                    <span class="text-danger">{{ $errors->first('libro') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoLibro.fields.libro_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="numeros_codigo">{{ trans('cruds.codigoLibro.fields.numeros_codigo') }}</label>
                <input class="form-control {{ $errors->has('numeros_codigo') ? 'is-invalid' : '' }}" type="number" name="numeros_codigo" id="numeros_codigo" value="{{ old('numeros_codigo', '') }}" step="1">
                @if($errors->has('numeros_codigo'))
                    <span class="text-danger">{{ $errors->first('numeros_codigo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoLibro.fields.numeros_codigo_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection