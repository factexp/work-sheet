@extends('layouts.admin')
@section('content')
@can('codigo_estudiante_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.codigo-estudiantes.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.codigoEstudiante.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.codigoEstudiante.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-CodigoEstudiante">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.codigoEstudiante.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.codigoEstudiante.fields.libro') }}
                        </th>
                        <th>
                            {{ trans('cruds.codigoEstudiante.fields.estudiante') }}
                        </th>
                        <th>
                            {{ trans('cruds.codigoEstudiante.fields.codigo') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($codigoEstudiantes as $key => $codigoEstudiante)
                        <tr data-entry-id="{{ $codigoEstudiante->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $codigoEstudiante->id ?? '' }}
                            </td>
                            <td>
                                {{ $codigoEstudiante->libro->nombre ?? '' }}
                            </td>
                            <td>
                                {{ $codigoEstudiante->estudiante->name ?? '' }}
                            </td>
                            <td>
                                {{ $codigoEstudiante->codigo ?? '' }}
                            </td>
                            <td>
                                @can('codigo_estudiante_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.codigo-estudiantes.show', $codigoEstudiante->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('codigo_estudiante_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.codigo-estudiantes.edit', $codigoEstudiante->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('codigo_estudiante_delete')
                                    <form action="{{ route('admin.codigo-estudiantes.destroy', $codigoEstudiante->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('codigo_estudiante_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.codigo-estudiantes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-CodigoEstudiante:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection