@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.codigoEstudiante.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.codigo-estudiantes.update", [$codigoEstudiante->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="libro_id">{{ trans('cruds.codigoEstudiante.fields.libro') }}</label>
                <select class="form-control select2 {{ $errors->has('libro') ? 'is-invalid' : '' }}" name="libro_id" id="libro_id">
                    @foreach($libros as $id => $entry)
                        <option value="{{ $id }}" {{ (old('libro_id') ? old('libro_id') : $codigoEstudiante->libro->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('libro'))
                    <span class="text-danger">{{ $errors->first('libro') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoEstudiante.fields.libro_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="estudiante_id">{{ trans('cruds.codigoEstudiante.fields.estudiante') }}</label>
                <select class="form-control select2 {{ $errors->has('estudiante') ? 'is-invalid' : '' }}" name="estudiante_id" id="estudiante_id">
                    @foreach($estudiantes as $id => $entry)
                        <option value="{{ $id }}" {{ (old('estudiante_id') ? old('estudiante_id') : $codigoEstudiante->estudiante->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('estudiante'))
                    <span class="text-danger">{{ $errors->first('estudiante') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoEstudiante.fields.estudiante_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="codigo">{{ trans('cruds.codigoEstudiante.fields.codigo') }}</label>
                <input class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" type="text" name="codigo" id="codigo" value="{{ old('codigo', $codigoEstudiante->codigo) }}">
                @if($errors->has('codigo'))
                    <span class="text-danger">{{ $errors->first('codigo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.codigoEstudiante.fields.codigo_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection