@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.estudiante.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.estudiantes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="usuario_id">{{ trans('cruds.estudiante.fields.usuario') }}</label>
                <select class="form-control select2 {{ $errors->has('usuario') ? 'is-invalid' : '' }}" name="usuario_id" id="usuario_id">
                    @foreach($usuarios as $id => $entry)
                        <option value="{{ $id }}" {{ old('usuario_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('usuario'))
                    <span class="text-danger">{{ $errors->first('usuario') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.estudiante.fields.usuario_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="curso_id">{{ trans('cruds.estudiante.fields.curso') }}</label>
                <select class="form-control select2 {{ $errors->has('curso') ? 'is-invalid' : '' }}" name="curso_id" id="curso_id">
                    @foreach($cursos as $id => $entry)
                        <option value="{{ $id }}" {{ old('curso_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('curso'))
                    <span class="text-danger">{{ $errors->first('curso') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.estudiante.fields.curso_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection