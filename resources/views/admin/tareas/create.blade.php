@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.tarea.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tareas.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="profesor_id">{{ trans('cruds.tarea.fields.profesor') }}</label>
                <select class="form-control select2 {{ $errors->has('profesor') ? 'is-invalid' : '' }}" name="profesor_id" id="profesor_id">
                    @foreach($profesors as $id => $entry)
                        <option value="{{ $id }}" {{ old('profesor_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('profesor'))
                    <span class="text-danger">{{ $errors->first('profesor') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.profesor_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="materia_id">{{ trans('cruds.tarea.fields.materia') }}</label>
                <select class="form-control select2 {{ $errors->has('materia') ? 'is-invalid' : '' }}" name="materia_id" id="materia_id">
                    @foreach($materias as $id => $entry)
                        <option value="{{ $id }}" {{ old('materia_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('materia'))
                    <span class="text-danger">{{ $errors->first('materia') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.materia_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="curso_id">{{ trans('cruds.tarea.fields.curso') }}</label>
                <select class="form-control select2 {{ $errors->has('curso') ? 'is-invalid' : '' }}" name="curso_id" id="curso_id">
                    @foreach($cursos as $id => $entry)
                        <option value="{{ $id }}" {{ old('curso_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('curso'))
                    <span class="text-danger">{{ $errors->first('curso') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.curso_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="nombre">{{ trans('cruds.tarea.fields.nombre') }}</label>
                <input class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" type="text" name="nombre" id="nombre" value="{{ old('nombre', '') }}" required>
                @if($errors->has('nombre'))
                    <span class="text-danger">{{ $errors->first('nombre') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.nombre_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="archivo">{{ trans('cruds.tarea.fields.archivo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('archivo') ? 'is-invalid' : '' }}" id="archivo-dropzone">
                </div>
                @if($errors->has('archivo'))
                    <span class="text-danger">{{ $errors->first('archivo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.archivo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="libro_id">{{ trans('cruds.tarea.fields.libro') }}</label>
                <select class="form-control select2 {{ $errors->has('libro') ? 'is-invalid' : '' }}" name="libro_id" id="libro_id">
                    @foreach($libros as $id => $entry)
                        <option value="{{ $id }}" {{ old('libro_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('libro'))
                    <span class="text-danger">{{ $errors->first('libro') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tarea.fields.libro_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.archivoDropzone = {
    url: '{{ route('admin.tareas.storeMedia') }}',
    maxFilesize: 10, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').find('input[name="archivo"]').remove()
      $('form').append('<input type="hidden" name="archivo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="archivo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($tarea) && $tarea->archivo)
      var file = {!! json_encode($tarea->archivo) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="archivo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection