@extends('layouts.admin')
@section('content')
<!DOCTYPE html>
<html>
<head>
<script src="https://es.liveworksheets.com/jquery.min.js"></script>
<meta http-equiv="Content-Language" content="es">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
  .capaiframe {
  	z-index: 0;
  	position: absolute;
   	margin-left: auto;
  	margin-right: auto;
  	left: 0;
  	right: 0;
  	top: 207px;
  	bottom:10px;
    border: 1px solid #999999;   
	box-shadow: 0px 0px 0px #666666; 
	background-color:#FFFFFF;

} 

.capatoolbar {
	z-index: 1;
  	position: absolute;
  	margin-left: auto;
  	margin-right: auto;
  	left: 0;
  	right: 0;
  	top: 170px;
  	background-color:#666666;
  	width: 1000px;
}

.boton {
border: 1px solid #666666;
}


.capaformulario {
display:none;
width:500px;
height:300px;
position: fixed;
margin-right:auto;
margin-left:auto;
right:0;
left:0;
z-index: 100;
top:200px;
background-color: #FFFFFF;
border: 1px solid #666666;
border-radius: 3px;
box-shadow: 0px 0px 8px #666666;
-moz-box-shadow: 0px 0px 8px #666666;
-webkit-box-shadow: 0px 0px 8px #666666;
}



</style>
<script language="javascript">
var hayerroresenlaficha = {{$idtarea}};
var hayerroresenlaficha = 0;

function previewWorksheet() {
document.getElementById("previewbutton").style.cursor = "wait";
document.body.style.cursor = "wait";
setTimeout(previewWorksheet2,10);
}

function previewWorksheet2() {
document.getElementById('iframe').contentWindow.rllnrlmth();


var contenidoaguardar = JSON.parse(localStorage.getItem("contenidoaguardar"));
	if (contenidoaguardar.length < 1) {
	alert("ERROR: Tienes que introducir al menos un cuadro de texto en tu ficha");
	document.getElementById("previewbutton").style.cursor = "pointer";
	} else if (hayerroresenlaficha == 0) {
		document.getElementById("iframe").src = "preview.asp";
		document.getElementById("previewbutton").style.backgroundColor = "#9FC4F9";
		document.getElementById("editbutton").style.backgroundColor = "#666666";
		document.getElementById("helpbutton").style.backgroundColor = "#666666";
		document.getElementById("savebutton").style.backgroundColor = "#666666";
		document.getElementById("discardbutton").style.backgroundColor = "#666666";
		document.getElementById("reuploadbutton").style.backgroundColor = "#666666";
		document.getElementById("savebutton").style.cursor = "pointer";
		document.getElementById("helpbutton").style.cursor = "pointer";
		document.getElementById("editbutton").style.cursor = "pointer";
		document.getElementById("previewbutton").style.cursor = "auto";
		document.getElementById("discardbutton").style.cursor = "pointer";
		document.getElementById("reuploadbutton").style.cursor = "pointer";
	} else {
	alert("Parece que hay errores en tu ficha. Por favor, compruébalo");
	document.getElementById("previewbutton").style.cursor = "pointer";
	}
	
document.body.style.cursor = "auto";

}

function editWorksheet() {
document.getElementById("iframe").src = "editor.asp?edit=";
document.getElementById("editbutton").style.backgroundColor = "#9FC4F9";
document.getElementById("previewbutton").style.backgroundColor = "#666666";
document.getElementById("helpbutton").style.backgroundColor = "#666666";
document.getElementById("savebutton").style.backgroundColor = "#666666";
document.getElementById("discardbutton").style.backgroundColor = "#666666";
document.getElementById("reuploadbutton").style.backgroundColor = "#666666";
document.getElementById("savebutton").style.cursor = "pointer";
document.getElementById("helpbutton").style.cursor = "pointer";
document.getElementById("editbutton").style.cursor = "auto";
document.getElementById("previewbutton").style.cursor = "pointer";
document.getElementById("discardbutton").style.cursor = "pointer";
document.getElementById("reuploadbutton").style.cursor = "pointer";
document.getElementById("drawbutton").style.display = "none";
document.getElementById("undobutton").style.display = "block";
document.getElementById("redobutton").style.display = "block";
}

function saveWorksheet() {
document.getElementById("savebutton").style.cursor = "wait";
document.body.style.cursor = "wait";
setTimeout(saveWorksheet2,10);
}


function saveWorksheet2() {	
document.getElementById('iframe').contentWindow.rllnrlmth();
//hayerroresenlaficha = document.getElementById('iframe').contentWindow.cmprbrrrs();
var contenidoaguardar = JSON.parse(localStorage.getItem("contenidoaguardar"));
	if (contenidoaguardar.length < 1) {
	alert("ERROR: Tienes que introducir al menos un cuadro de texto en tu ficha");
	document.getElementById("savebutton").style.cursor = "pointer";
	} else if (hayerroresenlaficha == 0) {
		var longitudcontenidoboxes = 0;
		for (var j=0; j < contenidoaguardar.length; j++){
			longitudcontenidoboxes = longitudcontenidoboxes + contenidoaguardar[j][0];
		}
		if (longitudcontenidoboxes < 1) {
			var confirmarguardado = confirm("Todos los cuadros de texto están en blanco. Tienes que poner las respuestas correctas en los cuadros para que funcione la auto-corrección en la ficha. ¿Seguro que quieres guardarla?");
			if (confirmarguardado == false) {	
                document.getElementById("savebutton").style.cursor = "pointer";		
	
			} 
		}

        var form = document.createElement("form");

   
        form._submit_function_ = form.submit;

        form.setAttribute("method", "get");
        form.setAttribute("action", "{{ route('admin.tareas.storetareadetalle') }}");

     
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "contenidoguardarFormularioSave");
        hiddenField.setAttribute("value", JSON.stringify(contenidoaguardar));

        form.appendChild(hiddenField);

		var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "idtarea");
        hiddenField.setAttribute("value", idtarea);

        form.appendChild(hiddenField);
        

        document.body.appendChild(form);
        form._submit_function_();
	} else {
	alert("Parece que hay errores en tu ficha. Por favor, compruébalo");
	document.getElementById("savebutton").style.cursor = "pointer";
	}
document.body.style.cursor = "auto";
}

function discardWorksheet() {
document.getElementById("editbutton").style.backgroundColor = "#666666";
document.getElementById("previewbutton").style.backgroundColor = "#666666";
document.getElementById("helpbutton").style.backgroundColor = "#666666";
document.getElementById("savebutton").style.backgroundColor = "#666666";
document.getElementById("discardbutton").style.backgroundColor = "#9FC4F9";
document.getElementById("reuploadbutton").style.backgroundColor = "#666666";
document.getElementById("savebutton").style.cursor = "pointer";
document.getElementById("helpbutton").style.cursor = "pointer";
document.getElementById("editbutton").style.cursor = "pointer";
document.getElementById("previewbutton").style.cursor = "pointer";
document.getElementById("discardbutton").style.cursor = "auto";
document.getElementById("reuploadbutton").style.cursor = "pointer";
}

function showAyuda() {
document.getElementById("iframe").src = "gettingstarted.asp";
document.getElementById("editbutton").style.backgroundColor = "#666666";
document.getElementById("previewbutton").style.backgroundColor = "#666666";
document.getElementById("helpbutton").style.backgroundColor = "#9FC4F9";
document.getElementById("savebutton").style.backgroundColor = "#666666";
document.getElementById("discardbutton").style.backgroundColor = "#666666";
document.getElementById("reuploadbutton").style.backgroundColor = "#666666";
document.getElementById("savebutton").style.cursor =  "pointer";
document.getElementById("helpbutton").style.cursor = "auto";
document.getElementById("editbutton").style.cursor =  "pointer";
document.getElementById("previewbutton").style.cursor =  "pointer";
document.getElementById("discardbutton").style.cursor = "pointer";
document.getElementById("reuploadbutton").style.cursor = "pointer";
}

function ejecutarDeshacer(){
document.getElementById('iframe').contentWindow.dshcr();
}

function ejecutarRehacer(){
document.getElementById('iframe').contentWindow.rhcr();
}



function showMenu() {
	document.getElementById("capatoolbar").innerHTML = '<div class="boton" id="savebutton" style="float:left;cursor:pointer" onclick="saveWorksheet()"><img src="images/iconoguardar.png" alt="Guardar" title="Guardar"><div style="float:left;width:300px;height:30px"></div><div class="boton" id="drawbutton" style="float:left;cursor:pointer;display:none" onclick="ejecutarDraw()"><img src="images/iconopencil.png" alt="Dibujar" title="Dibujar"></div><div class="boton" id="borrarbutton" style="float:left;cursor:pointer;display:none" onclick="ejecutarBorrar()"><img src="images/iconoeraser.png" alt="Borrar" title="Borrar"></div><div id="palettebutton" style="display:none;float:left;cursor:pointer;border:1px solid #000000;background-color:#0000FF;width:30px;height:30px" onclick="mostrarPaleta()"></div><div class="boton" id="undobutton" style="float:left;cursor:pointer" onclick="ejecutarDeshacer()"><img src="images/iconodeshacer.png" alt="Deshacer" title="Deshacer"></div><div class="boton" id="redobutton" style="float:left;cursor:pointer" onclick="ejecutarRehacer()"><img src="images/iconorehacer.png" alt="Rehacer" title="Rehacer"></div>'
}

function hideMenu() {
	document.getElementById("capatoolbar").innerHTML = ''
}

function discardWorksheet() {
var r = confirm("¿Seguro que quieres descartar esta ficha?");
	if (r == true) {
		localStorage.setItem("contenidoaguardar", "");
		document.getElementById("iframe").src = "uploadworksheet1.asp?clearcookies=true";
	} 

}

function reuploadWorksheet() {


		document.getElementById("iframe").src = "uploadworksheet1.asp?updating=true";


		document.getElementById("reuploadbutton").style.backgroundColor = "#9FC4F9";
		document.getElementById("previewbutton").style.backgroundColor = "#666666";
		document.getElementById("editbutton").style.backgroundColor = "#666666";
		document.getElementById("helpbutton").style.backgroundColor = "#666666";
		document.getElementById("savebutton").style.backgroundColor = "#666666";
		document.getElementById("discardbutton").style.backgroundColor = "#666666";
		document.getElementById("reuploadbutton").style.cursor = "auto";
		document.getElementById("savebutton").style.cursor = "pointer";
		document.getElementById("helpbutton").style.cursor = "pointer";
		document.getElementById("editbutton").style.cursor = "pointer";
		document.getElementById("previewbutton").style.cursor = "pointer";
		document.getElementById("discardbutton").style.cursor = "pointer";
}

function ocultarelformulario() {
document.getElementById("capaformulario").style.display = "none"
}



function comprobarerroresficha() {
hayerroresenlaficha = 0;
var contenidoaguardar = JSON.parse(localStorage.getItem("contenidoaguardar"));
	if (contenidoaguardar.length < 1) {
	alert("ERROR: Tienes que introducir al menos un cuadro de texto en tu ficha");
	hayerroresenlaficha = 1;
	}
	for (var j=0; j < contenidoaguardar.length; j++){
		if (contenidoaguardar[j][6] == 1) {
			hayerroresenlaficha = 1;
		}
	}
	
	if (contenidoaguardar.length > 0 && hayerroresenlaficha == 1) {
		alert("Parece que hay errores en tu ficha. Por favor, compruébalo");
	}
}






function localstorageTest(){
    var test = 'test';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch(e) {
        return false;
    }
}


function validartitulo() {
var valorintroducido = document.getElementById("title").value;
var nuevovalor = valorintroducido.replace(/["<>#%{}|\^~`;/$*@=&_]/g, '-');
document.getElementById("title").value = nuevovalor;
}



function comprobarRequisitos() {

	if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){
	alert("El editor de Live Worksheets no funciona en dispositivos táctiles. Las fichas interactivas son compatibles con tablets y teléfonos móviles, pero necesitas un PC con teclado y ratón para prepararlas.");
	}
	if (navigator.cookieEnabled) {
	cookieshabilidadas = 1;
	} else {
	alert("El editor de Live Worksheets necesita cookies para funcionar. Por favor, habilita las cookies en tu navegador");
	}
	if(localstorageTest() === false){
	alert("El editor de Live Worksheets no funciona sin almacenamiento interno. Por favor, habilita el almacenamiento interno en tu navegador");
	}
}


</script>
<script language="javascript">
var idtarea = "{{$idtarea}}";
var mostrandologin = 0;
var mostrandomenuteachers = 0;
var mostrandomenuworksheets = 0;
var mostrandomenuworkbooks = 0;

function mostrarLogin() {
	if (mostrandologin < 1) {
		document.getElementById("capalogin").style.display = "block";
		mostrandologin = 1;
	} else {
		document.getElementById("capalogin").style.display = "none";
		mostrandologin = 0;
	}
}

function mostrarLogin2() {
	if (mostrandologin < 1) {
		document.getElementById("teachersloginspan").style.display = "block";
		mostrandologin = 1;
	} else {
		document.getElementById("teachersloginspan").style.display = "none";
		mostrandologin = 0;
	}
}

function closeLogin() {
		document.getElementById("capalogin").style.display = "none";
		mostrandologin = 0;
}


function ejecutaronload() {

thisurl = window.location.href;
separarurl = thisurl.split("liveworksheets.co");


}

var idiomasdebusquedaordenados = 0;

function mostrarbusquedaavanzada() {
	if (document.getElementById("capabusquedaavanzada").offsetHeight < 100) {
		$( "#capabusquedaavanzada").animate({
			height: "250px"
		}, 500, function() {
			window.addEventListener('click', clickedoutsidebusquedaavanzada, false);
		});
	} else {
		$( "#capabusquedaavanzada").animate({
			height: "0px"
		}, 500, function() {
			window.removeEventListener('click', clickedoutsidebusquedaavanzada, false);
		});
	}
	if (idiomasdebusquedaordenados == 0) {
		ordenarIdiomasBusqueda();
		idiomasdebusquedaordenados = 1;
	}
}
 
var clickedoutsidebusquedaavanzada = function (event) {
	if (document.getElementById('capabusquedaavanzada').contains(event.target)){
		
	} else{
		mostrarbusquedaavanzada();
		
	}
};


function desplegarmenuteachers(pixeles) {
	if (document.getElementById("teachersmenu").offsetHeight < 50) {
		$( "#teachersmenu").animate({
			height:  pixeles + "px"
		}, 500, function() {
			document.getElementById("teachersdropdowntriangle").style.transform = "rotate(180deg)";// Animation complete.
			window.addEventListener('click', clickedoutsideteachersmenu, false);
		});
		
	} else {
		$( "#teachersmenu").animate({
			height: "36px"
		}, 500, function() {
			document.getElementById("teachersdropdowntriangle").style.transform = "rotate(0deg)";// Animation complete.
		});
	}
}

function desplegarmenuteacherssinodesplegado(pixeles) {
	if (document.getElementById("teachersmenu").offsetHeight < 50) {
		$( "#teachersmenu").animate({
			height:  pixeles + "px"
		}, 500, function() {
			document.getElementById("teachersdropdowntriangle").style.transform = "rotate(180deg)";// Animation complete.
			window.addEventListener('click', clickedoutsideteachersmenu, false);
		});
	}
}

var clickedoutsideteachersmenu = function (event) {
	if (document.getElementById('teachersmenu').contains(event.target)){
		
	} else{
		desplegarmenuteachers(36);
		window.removeEventListener('click', clickedoutsideteachersmenu, false);
	}
};


function desplegarmakeinteractiveworksheets(pixeles) {
	if (document.getElementById("makeinteractiveworksheetsdiv").offsetHeight < 50) {
		$( "#makeinteractiveworksheetsdiv").animate({
			height:  pixeles + "px"
		}, 500, function() {
			window.addEventListener('click', clickedoutsidemakeinteractiveworksheets, false);
		});
		
	} else {
		$( "#makeinteractiveworksheetsdiv").animate({
			height: "30px"
		}, 500, function() {

		});
	}
}

var clickedoutsidemakeinteractiveworksheets = function (event) {
	if (document.getElementById('makeinteractiveworksheetsdiv').contains(event.target)){
		
	} else{
		desplegarmakeinteractiveworksheets(30);
		window.removeEventListener('click', clickedoutsidemakeinteractiveworksheets, false);
	}
};


function desplegarmakeinteractiveworkbooks(pixeles) {
	if (document.getElementById("makeinteractiveworkbooksdiv").offsetHeight < 50) {
		$( "#makeinteractiveworkbooksdiv").animate({
			height:  pixeles + "px"
		}, 500, function() {
			window.addEventListener('click', clickedoutsidemakeinteractiveworkbooks, false);
		});
		
	} else {
		$( "#makeinteractiveworkbooksdiv").animate({
			height: "30px"
		}, 500, function() {

		});
	}
}

var clickedoutsidemakeinteractiveworkbooks = function (event) {
	if (document.getElementById('makeinteractiveworkbooksdiv').contains(event.target)){
		
	} else{
		desplegarmakeinteractiveworkbooks(30);
		window.removeEventListener('click', clickedoutsidemakeinteractiveworkbooks, false);
	}
};


 
var mostusedlangs = [["English","en"],["Spanish","es"],["Malay","ms"],["Arabic","ar"],["Indonesian","id"],["Portuguese","pt"],["Thai","th"],["Chinese","zh"],["Catalan","ca"],["Greek (modern)","el"],["Italian","it"],["French","fr"],["Romanian","ro"],["Tamil","ta"],["Basque","eu"]];

function ordenarIdiomasBusqueda() {
	var opcionesmostradas = 0;
	var selElem = document.getElementById("languagebusqueda");
    var tmpAry = new Array();
    for (var i=0;i<selElem.options.length;i++) {
        tmpAry[i] = new Array();
        tmpAry[i][0] = selElem.options[i].text;
        tmpAry[i][1] = selElem.options[i].value;
    }
    tmpAry.sort();
    while (selElem.options.length > 0) {
        selElem.options[0] = null;
    }
    
    for (var i=0;i<tmpAry.length;i++) {
    	if (tmpAry[i][1].toLowerCase() == "") {
	        var op = new Option(tmpAry[i][0], tmpAry[i][1]);
	        selElem.options[0] = op;
	        opcionesmostradas++;
        }
    }
    
    for (var i=0;i<mostusedlangs.length;i++) {
	        var op = new Option(mostusedlangs[i][0], mostusedlangs[i][1]);
	        selElem.options[opcionesmostradas] = op;
	        opcionesmostradas++;
    }
        
    for (var i=0;i<tmpAry.length;i++) {
        var op = new Option(tmpAry[i][0], tmpAry[i][1]);
        selElem.options[opcionesmostradas] = op;
        opcionesmostradas++;
    }
    return;
}

var numerodemensajesemergentes = 0;

function mensajeemergente(texto,anchuramensaje,alturamensaje) {
	var elemDiv = document.createElement('div');
	elemDiv.setAttribute("id", "mensajeemergente" + numerodemensajesemergentes);
	elemDiv.style.cssText = ' padding-left: 10px; padding-right: 10px;position:fixed;width:' + anchuramensaje + 'px;height:' + alturamensaje + 'px;z-index: 36000000;top:0;bottom:0;left:0;right:0;margin-top:auto;margin-bottom:auto;margin-left:auto;margin-right:auto; background-color: #FFFFFF;border: 1px solid #999999;border-radius: 0px;box-shadow: 0px 0px 8px #999999;';
	var elhtml = '<div style="position:absolute;top:5px;right:5px;width:15px;height:20px;"><span class="enlace" style="font-size:16px" onclick="ocultarmensajeemergente(' + numerodemensajesemergentes + ')">x</span></div>';
	elhtml = elhtml + '<table width="100%" height="100%"><tr><td valign="middle" align="center">';
	elhtml = elhtml + texto;
	elhtml = elhtml + '</td></tr></table>';
	elemDiv.innerHTML = elhtml;
	document.body.appendChild(elemDiv);
	numerodemensajesemergentes = numerodemensajesemergentes + 1;
}

function ocultarmensajeemergente(numero) {
    var element = document.getElementById("mensajeemergente" + numero);
    element.parentNode.removeChild(element);
}


</script>



</head>

<body onload="comprobarRequisitos(),ejecutaronload()">


<div id="capatoolbar" class="capatoolbar">
</div>


<div id="capaiframe" class="capaiframe" style="width:1000px">
	<iframe id="iframe" src="{{ route('admin.tareas.editor', $idtarea) }}" style="width:1000px;height:100%;border:0px;overflow:scroll" scrolling="auto"></iframe>
</div>


<div id="capaformulario" class="capaformulario">
<div align="center">
<table width="500">
<tr><td>
<form method="post" action="saveworksheet.asp" name="formulario" id="formulario">
<p align="center"><font color="#666666" face="Arial">
Por favor, introduce un título para tu ficha:<br>
<input type="text" name="title" id="title" value="" size="50" onblur="validartitulo()"><br><br>
Por favor, confirma tu nombre de usuario y contraseña para guardar esta ficha.:<br>
Usuario: <input type="text" name="usernamelogin" size="20">
<br>
Contraseña: <input type="password" name="passwordlogin" size="20"><br><br>
<input type="hidden" name="contenidojsonaguardar" id="contenidojsonaguardar" value="">
<input type="hidden" name="editandoid" id="editandoid" value="">
<input type="hidden" name="defaultfontfamily" id="defaultfontfamily" value="">
<input type="hidden" name="defaultfontsize" id="defaultfontsize" value="">
<input type="hidden" name="defaultfontcolor" id="defaultfontcolor" value="">
<input type="hidden" name="defaultbgcolorred" id="defaultbgcolorred" value="">
<input type="hidden" name="defaultbgcolorgreen" id="defaultbgcolorgreen" value="">
<input type="hidden" name="defaultbgcolorblue" id="defaultbgcolorblue" value="">
<input type="hidden" name="defaultbgopacity" id="defaultbgopacity" value="">
<input type="hidden" name="defaultbordercolor" id="defaultbordercolor" value="">
<input type="hidden" name="defaultborderwidth" id="defaultborderwidth" value="">
<input type="hidden" name="defaultroundedcorners" id="defaultroundedcorners" value="">
<input type="hidden" name="defaulttextalign" id="defaulttextalign" value="">
<input type="submit" value="Guardar" name="B1"></font></p>
<p align="center"><font face="Arial" color="#666666"><span class="enlace" onclick="ocultarelformulario()">
Close</span></font></p>
</form>
</td></tr>
</table>
</div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2079983-7', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>


@endsection