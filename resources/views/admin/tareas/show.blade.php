@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.tarea.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tareas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.id') }}
                        </th>
                        <td>
                            {{ $tarea->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.profesor') }}
                        </th>
                        <td>
                            {{ $tarea->profesor->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.materia') }}
                        </th>
                        <td>
                            {{ $tarea->materia->nombre ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.curso') }}
                        </th>
                        <td>
                            {{ $tarea->curso->nombre ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.nombre') }}
                        </th>
                        <td>
                            {{ $tarea->nombre }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.archivo') }}
                        </th>
                        <td>
                            @if($tarea->archivo)
                                <a href="{{ $tarea->archivo->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tarea.fields.libro') }}
                        </th>
                        <td>
                            {{ $tarea->libro->nombre ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tareas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection