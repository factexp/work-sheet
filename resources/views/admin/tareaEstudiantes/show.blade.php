@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.tareaEstudiante.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tarea-estudiantes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.id') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.tarea') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->tarea->nombre ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.valor') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->valor }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.estudiante') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->estudiante->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.profesor') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->profesor->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.curso') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->curso->nombre ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tareaEstudiante.fields.materia') }}
                        </th>
                        <td>
                            {{ $tareaEstudiante->materia->nombre ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tarea-estudiantes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection