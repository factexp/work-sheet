@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.tareaEstudiante.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tarea-estudiantes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="tarea_id">{{ trans('cruds.tareaEstudiante.fields.tarea') }}</label>
                <select class="form-control select2 {{ $errors->has('tarea') ? 'is-invalid' : '' }}" name="tarea_id" id="tarea_id" required>
                    @foreach($tareas as $id => $entry)
                        <option value="{{ $id }}" {{ old('tarea_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('tarea'))
                    <span class="text-danger">{{ $errors->first('tarea') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tareaEstudiante.fields.tarea_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="valor">{{ trans('cruds.tareaEstudiante.fields.valor') }}</label>
                <input class="form-control {{ $errors->has('valor') ? 'is-invalid' : '' }}" type="text" name="valor" id="valor" value="{{ old('valor', '') }}">
                @if($errors->has('valor'))
                    <span class="text-danger">{{ $errors->first('valor') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tareaEstudiante.fields.valor_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection