@extends('layouts.admin')
@section('content')
@can('tarea_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.tareas.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.tarea.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.tarea.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Tarea">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.profesor') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.materia') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.curso') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.nombre') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.archivo') }}
                        </th>
                        <th>
                            {{ trans('cruds.tarea.fields.libro') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tareas as $key => $tarea)
                        <tr data-entry-id="{{ $tarea->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $tarea->id ?? '' }}
                            </td>
                            <td>
                                {{ $tarea->profesor->name ?? '' }}
                            </td>
                            <td>
                                {{ $tarea->materia->nombre ?? '' }}
                            </td>
                            <td>
                                {{ $tarea->curso->nombre ?? '' }}
                            </td>
                            <td>
                                {{ $tarea->nombre ?? '' }}
                            </td>
                            <td>
                                @if($tarea->archivo)
                                    <a href="{{ $tarea->archivo->getUrl() }}" target="_blank">
                                        {{ trans('global.view_file') }}
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $tarea->libro->nombre ?? '' }}
                            </td>
                            <td>                              
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.tareas.realizarTareaEstudiante', $tarea->id) }}">
                                    Seleccionar
                                </a>             


                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('tarea_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.tareas.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Tarea:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection