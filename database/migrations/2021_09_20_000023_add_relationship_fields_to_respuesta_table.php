<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRespuestaTable extends Migration
{
    public function up()
    {
        Schema::table('respuesta', function (Blueprint $table) {
            $table->unsignedBigInteger('tarea_id')->nullable();
            $table->foreign('tarea_id', 'tarea_fk_4875176')->references('id')->on('tareas');
        });
    }
}
