<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCodigoLibrosTable extends Migration
{
    public function up()
    {
        Schema::table('codigo_libros', function (Blueprint $table) {
            $table->unsignedBigInteger('libro_id')->nullable();
            $table->foreign('libro_id', 'libro_fk_4915365')->references('id')->on('libros');
        });
    }
}
