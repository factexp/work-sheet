<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametroRespuestaTable extends Migration
{
    public function up()
    {
        Schema::create('parametro_respuesta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('valor')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
