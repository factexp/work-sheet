<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToParametroRespuestaTable extends Migration
{
    public function up()
    {
        Schema::table('parametro_respuesta', function (Blueprint $table) {
            $table->unsignedBigInteger('respuesta_id')->nullable();
            $table->foreign('respuesta_id', 'respuesta_fk_4875216')->references('id')->on('respuesta');
        });
    }
}
