<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTareaEstudiantesTable extends Migration
{
    public function up()
    {
        Schema::table('tarea_estudiantes', function (Blueprint $table) {
            $table->unsignedBigInteger('tarea_id');
            $table->foreign('tarea_id', 'tarea_fk_4849858')->references('id')->on('tareas');
            $table->unsignedBigInteger('estudiante_id')->nullable();
            $table->foreign('estudiante_id', 'estudiante_fk_4849860')->references('id')->on('users');
            $table->unsignedBigInteger('profesor_id')->nullable();
            $table->foreign('profesor_id', 'profesor_fk_4849861')->references('id')->on('users');
            $table->unsignedBigInteger('curso_id')->nullable();
            $table->foreign('curso_id', 'curso_fk_4849862')->references('id')->on('cursos');
            $table->unsignedBigInteger('materia_id')->nullable();
            $table->foreign('materia_id', 'materia_fk_4849867')->references('id')->on('materia');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4849866')->references('id')->on('users');
        });
    }
}
