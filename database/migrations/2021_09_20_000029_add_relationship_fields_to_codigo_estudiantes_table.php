<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCodigoEstudiantesTable extends Migration
{
    public function up()
    {
        Schema::table('codigo_estudiantes', function (Blueprint $table) {
            $table->unsignedBigInteger('libro_id')->nullable();
            $table->foreign('libro_id', 'libro_fk_4915372')->references('id')->on('libros');
            $table->unsignedBigInteger('estudiante_id')->nullable();
            $table->foreign('estudiante_id', 'estudiante_fk_4915373')->references('id')->on('users');
        });
    }
}
