<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToLibrosTable extends Migration
{
    public function up()
    {
        Schema::table('libros', function (Blueprint $table) {
            $table->unsignedBigInteger('curso_id')->nullable();
            $table->foreign('curso_id', 'curso_fk_4915346')->references('id')->on('cursos');
        });
    }
}
