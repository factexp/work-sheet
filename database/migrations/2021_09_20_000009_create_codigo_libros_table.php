<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodigoLibrosTable extends Migration
{
    public function up()
    {
        Schema::create('codigo_libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('numeros_codigo')->nullable();
            $table->string('codigo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
