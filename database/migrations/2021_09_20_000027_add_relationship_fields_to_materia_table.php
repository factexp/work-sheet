<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMateriaTable extends Migration
{
    public function up()
    {
        Schema::table('materia', function (Blueprint $table) {
            $table->unsignedBigInteger('curso_id')->nullable();
            $table->foreign('curso_id', 'curso_fk_4849845')->references('id')->on('cursos');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4849846')->references('id')->on('users');
        });
    }
}
