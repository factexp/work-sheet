<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodigoEstudiantesTable extends Migration
{
    public function up()
    {
        Schema::create('codigo_estudiantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
