<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTareasTable extends Migration
{
    public function up()
    {
        Schema::table('tareas', function (Blueprint $table) {
            $table->unsignedBigInteger('profesor_id')->nullable();
            $table->foreign('profesor_id', 'profesor_fk_4849848')->references('id')->on('users');
            $table->unsignedBigInteger('materia_id')->nullable();
            $table->foreign('materia_id', 'materia_fk_4849849')->references('id')->on('materia');
            $table->unsignedBigInteger('curso_id')->nullable();
            $table->foreign('curso_id', 'curso_fk_4849850')->references('id')->on('cursos');
            $table->unsignedBigInteger('libro_id')->nullable();
            $table->foreign('libro_id', 'libro_fk_4915363')->references('id')->on('libros');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4849854')->references('id')->on('users');
        });
    }
}
