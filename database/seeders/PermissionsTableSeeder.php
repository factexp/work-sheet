<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'audit_log_show',
            ],
            [
                'id'    => 18,
                'title' => 'audit_log_access',
            ],
            [
                'id'    => 19,
                'title' => 'curso_create',
            ],
            [
                'id'    => 20,
                'title' => 'curso_edit',
            ],
            [
                'id'    => 21,
                'title' => 'curso_show',
            ],
            [
                'id'    => 22,
                'title' => 'curso_delete',
            ],
            [
                'id'    => 23,
                'title' => 'curso_access',
            ],
            [
                'id'    => 24,
                'title' => 'materium_create',
            ],
            [
                'id'    => 25,
                'title' => 'materium_edit',
            ],
            [
                'id'    => 26,
                'title' => 'materium_show',
            ],
            [
                'id'    => 27,
                'title' => 'materium_delete',
            ],
            [
                'id'    => 28,
                'title' => 'materium_access',
            ],
            [
                'id'    => 29,
                'title' => 'profesor_access',
            ],
            [
                'id'    => 30,
                'title' => 'tarea_create',
            ],
            [
                'id'    => 31,
                'title' => 'tarea_edit',
            ],
            [
                'id'    => 32,
                'title' => 'tarea_show',
            ],
            [
                'id'    => 33,
                'title' => 'tarea_delete',
            ],
            [
                'id'    => 34,
                'title' => 'tarea_access',
            ],
            [
                'id'    => 35,
                'title' => 'estudiante_parent_access',
            ],
            [
                'id'    => 36,
                'title' => 'tarea_estudiante_create',
            ],
            [
                'id'    => 37,
                'title' => 'tarea_estudiante_edit',
            ],
            [
                'id'    => 38,
                'title' => 'tarea_estudiante_show',
            ],
            [
                'id'    => 39,
                'title' => 'tarea_estudiante_delete',
            ],
            [
                'id'    => 40,
                'title' => 'tarea_estudiante_access',
            ],
            [
                'id'    => 41,
                'title' => 'estudiante_create',
            ],
            [
                'id'    => 42,
                'title' => 'estudiante_edit',
            ],
            [
                'id'    => 43,
                'title' => 'estudiante_show',
            ],
            [
                'id'    => 44,
                'title' => 'estudiante_delete',
            ],
            [
                'id'    => 45,
                'title' => 'estudiante_access',
            ],
            [
                'id'    => 46,
                'title' => 'reporte_access',
            ],
            [
                'id'    => 47,
                'title' => 'representante_create',
            ],
            [
                'id'    => 48,
                'title' => 'representante_edit',
            ],
            [
                'id'    => 49,
                'title' => 'representante_show',
            ],
            [
                'id'    => 50,
                'title' => 'representante_delete',
            ],
            [
                'id'    => 51,
                'title' => 'representante_access',
            ],
            [
                'id'    => 52,
                'title' => 'respuestum_access',
            ],
            [
                'id'    => 53,
                'title' => 'parametro_respuestum_create',
            ],
            [
                'id'    => 54,
                'title' => 'parametro_respuestum_edit',
            ],
            [
                'id'    => 55,
                'title' => 'parametro_respuestum_show',
            ],
            [
                'id'    => 56,
                'title' => 'parametro_respuestum_delete',
            ],
            [
                'id'    => 57,
                'title' => 'parametro_respuestum_access',
            ],
            [
                'id'    => 58,
                'title' => 'libro_create',
            ],
            [
                'id'    => 59,
                'title' => 'libro_edit',
            ],
            [
                'id'    => 60,
                'title' => 'libro_show',
            ],
            [
                'id'    => 61,
                'title' => 'libro_delete',
            ],
            [
                'id'    => 62,
                'title' => 'libro_access',
            ],
            [
                'id'    => 63,
                'title' => 'libros_parent_access',
            ],
            [
                'id'    => 64,
                'title' => 'codigo_libro_create',
            ],
            [
                'id'    => 65,
                'title' => 'codigo_libro_access',
            ],
            [
                'id'    => 66,
                'title' => 'codigo_estudiante_create',
            ],
            [
                'id'    => 67,
                'title' => 'codigo_estudiante_edit',
            ],
            [
                'id'    => 68,
                'title' => 'codigo_estudiante_show',
            ],
            [
                'id'    => 69,
                'title' => 'codigo_estudiante_delete',
            ],
            [
                'id'    => 70,
                'title' => 'codigo_estudiante_access',
            ],
            [
                'id'    => 71,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
